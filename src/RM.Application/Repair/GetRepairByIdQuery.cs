﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Repair;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Repair
{
    internal class GetRepairByIdQuery : IRequestHandler<GetRepairByIdCriteria, GetRepairByIdResult>
    {
        private readonly IRepository<TRepair> repairRepository;

        public GetRepairByIdQuery(IRepository<TRepair> repairRepository)
        {
            this.repairRepository = repairRepository;
        }

        public async Task<GetRepairByIdResult> Handle(GetRepairByIdCriteria request, CancellationToken cancellationToken)
        {
            var item = repairRepository.GetItem(request.Id);
            var result = new GetRepairByIdResult
            {
                Id = item.Id,
                Mileage = item.Mileage,
                VehicleId = item.Vehicle.Id,
                DriverId = item.Driver.Id,
                Description = item.Description,
                IsClosed = item.IsClosed,
                MasterId = item.Master?.Id,
                CreatedOn = item.CreatedOn,
                ClosedOn = item.ClosedOn
            };
            return result;
        }
    }
}
