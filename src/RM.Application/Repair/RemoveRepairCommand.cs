﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Repair;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Repair
{
    internal class RemoveRepairCommand : IRequestHandler<RemoveRepairCriteria>
    {
        private readonly IRepository<TRepair> repairRepository;

        public RemoveRepairCommand(IRepository<TRepair> repairRepository)
        {
            this.repairRepository = repairRepository;
        }

        public async Task<Unit> Handle(RemoveRepairCriteria request, CancellationToken cancellationToken)
        {
            repairRepository.Remove(request.Id);
            return new Unit();
        }
    }
}
