﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Repair;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Repair
{
    internal class CloseRepairCommand : IRequestHandler<CloseRepairCriteria>
    {
        private readonly IRepository<TRepair> repairRepository;
        private readonly IRepository<TEmployee> employeeRepository;

        public CloseRepairCommand(
            IRepository<TEmployee> employeeRepository,
            IRepository<TRepair> repairRepository)
        {
            this.employeeRepository = employeeRepository;
            this.repairRepository = repairRepository;
        }

        public async Task<Unit> Handle(CloseRepairCriteria request, CancellationToken cancellationToken)
        {
            var item = repairRepository.GetItem(request.Id);
            item.IsClosed = true;
            item.Master = employeeRepository.GetItem(request.MasterId);
            item.ClosedOn = request.ClosedOn;
            repairRepository.Update(item);
            return new Unit();
        }
    }
}
