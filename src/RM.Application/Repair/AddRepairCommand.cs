﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Repair;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Repair
{
    internal class AddRepairCommand : IRequestHandler<AddRepairCriteria>
    {
        private readonly IRepository<TRepair> repairRepository;
        private readonly IRepository<TVehicle> vehicleRepository;
        private readonly IRepository<TEmployee> employeeRepository;

        public AddRepairCommand(
            IRepository<TRepair> repairRepository,
            IRepository<TVehicle> vehicleRepository,
            IRepository<TEmployee> employeeRepository)
        {
            this.repairRepository = repairRepository;
            this.vehicleRepository = vehicleRepository;
            this.employeeRepository = employeeRepository;
        }

        public async Task<Unit> Handle(AddRepairCriteria request, CancellationToken cancellationToken)
        {
            var item = new TRepair
            {
                Vehicle = vehicleRepository.GetItem(request.VehicleId),
                Description = request.Description,
                Driver = employeeRepository.GetItem(request.DriverId),
                Mileage = request.Mileage,
                IsClosed = false,
                CreatedOn = request.CreatedOn,
                ClosedOn = DateTime.Now
            };

            repairRepository.Create(item);

            return new Unit();
        }
    }
}
