﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Repair;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Repair
{
    internal class UpdateNewRepairCommand : IRequestHandler<UpdateRepairCriteria>
    {
        private readonly IRepository<TRepair> repairRepository;
        private readonly IRepository<TVehicle> vehicleRepository;
        private readonly IRepository<TEmployee> employeeRepository;

        public UpdateNewRepairCommand(
            IRepository<TRepair> repairRepository,
            IRepository<TVehicle> vehicleRepository,
            IRepository<TEmployee> employeeRepository)
        {
            this.repairRepository = repairRepository;
            this.vehicleRepository = vehicleRepository;
            this.employeeRepository = employeeRepository;
        }

        public async Task<Unit> Handle(UpdateRepairCriteria request, CancellationToken cancellationToken)
        {
            var item = repairRepository.GetItem(request.Id);

            item.Vehicle = vehicleRepository.GetItem(request.VehicleId);
            item.Description = request.Description;
            item.Driver = employeeRepository.GetItem(request.DriverId);
            item.Mileage = request.Mileage;
            item.CreatedOn = request.CreatedOn;
            repairRepository.Update(item);

            return new Unit();
        }
    }
}
