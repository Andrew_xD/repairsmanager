﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Work;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Work
{
    internal class RemoveWorkCommand : IRequestHandler<RemoveWorkCriteria>
    {
        private readonly IRepository<TWork> workRepository;

        public RemoveWorkCommand(IRepository<TWork> workRepository)
        {
            this.workRepository = workRepository;
        }

        public async Task<Unit> Handle(RemoveWorkCriteria request, CancellationToken cancellationToken)
        {
            workRepository.Remove(request.Id);
            return new Unit();
        }
    }
}
