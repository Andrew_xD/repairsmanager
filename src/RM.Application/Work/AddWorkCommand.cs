﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Work;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Work
{
    public class AddWorkCommand : IRequestHandler<AddWorkCriteria>
    {
        private IRepository<TWork> workRepository;
        private IRepository<TRepair> repairRepository;

        public AddWorkCommand(
            IRepository<TWork> workRepository,
            IRepository<TRepair> repairRepository)
        {
            this.workRepository = workRepository;
            this.repairRepository = repairRepository;
        }

        public async Task<Unit> Handle(AddWorkCriteria request, CancellationToken cancellationToken)
        {
            var item = new TWork
            {
                Amount = request.Amount,
                Description = request.Description,
                Repair = repairRepository.GetItem(request.RepairId)
            };
            workRepository.Create(item);
            return new Unit();
        }
    }
}
