﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Work;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Work
{
    internal class UpdateWorkCommand : IRequestHandler<UpdateWorkCriteria>
    {
        private readonly IRepository<TWork> workRepository;

        public UpdateWorkCommand(IRepository<TWork> workRepository, IRepository<TRepair> repairRepository)
        {
            this.workRepository = workRepository;
        }

        public async Task<Unit> Handle(UpdateWorkCriteria request, CancellationToken cancellationToken)
        {
            var item = workRepository.GetItem(request.Id);
            item.Amount = request.Amount;
            item.Description = request.Description;
            workRepository.Update(item);

            return new Unit();
        }
    }
}
