﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Work;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Work
{
    internal class GetWorkByRepairIdQuery : IRequestHandler<GetWorkByRepairIdCriteria, IEnumerable<GetWorkByRepairIdResult>>
    {
        private readonly IRepository<TWork> workRepository;

        public GetWorkByRepairIdQuery(IRepository<TWork> workRepository)
        {
            this.workRepository = workRepository;
        }

        public async Task<IEnumerable<GetWorkByRepairIdResult>> Handle(GetWorkByRepairIdCriteria request, CancellationToken cancellationToken)
        {
            return workRepository.GetItems(x => x.Repair.Id == request.Id).Select(it => new GetWorkByRepairIdResult
            {
                Id = it.Id,
                Amount = it.Amount,
                Description = it.Description
            });
        }
    }
}
