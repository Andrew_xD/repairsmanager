﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Model;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Model
{
    internal class GetModelByIdQuery : IRequestHandler<GetModelByIdCriteria, GetModelResult>
    {
        private readonly IRepository<TVehicleModel> modelRepository;

        public GetModelByIdQuery(IRepository<TVehicleModel> modelRepository)
        {
            this.modelRepository = modelRepository;
        }

        public async Task<GetModelResult> Handle(GetModelByIdCriteria request, CancellationToken cancellationToken)
        {
            var item = modelRepository.GetItem(request.Id);
            var result = new GetModelResult
            {
                Id = item.Id,
                Name = item.Name,
                MarkId = item.Mark.Id
            };
            return result;
        }
    }
}
