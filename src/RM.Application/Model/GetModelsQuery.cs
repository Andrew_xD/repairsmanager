﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Model;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Model
{
    internal class GetModelsQuery : IRequestHandler<GetModelsCriteria, IEnumerable<GetModelResult>>
    {
        private readonly IRepository<TVehicleModel> modelRepository;

        public GetModelsQuery(IRepository<TVehicleModel> modelRepository)
        {
            this.modelRepository = modelRepository;
        }

        public async Task<IEnumerable<GetModelResult>> Handle(GetModelsCriteria request, CancellationToken cancellationToken)
        {
            var items = modelRepository.GetItems().Select(x => new GetModelResult
            {
                Id = x.Id,
                Name = x.Name,
                MarkId = x.Mark.Id
            });
            return items;
        }
    }
}
