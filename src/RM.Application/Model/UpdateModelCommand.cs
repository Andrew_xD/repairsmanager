﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Model;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Model
{
    internal class UpdateModelCommand : IRequestHandler<UpdateModelCriteria>
    {
        private readonly IRepository<TVehicleModel> modelRepository;

        public UpdateModelCommand(IRepository<TVehicleModel> modelRepository)
        {
            this.modelRepository = modelRepository;
        }

        public async Task<Unit> Handle(UpdateModelCriteria request, CancellationToken cancellationToken)
        {
            var item = modelRepository.GetItem(request.Id);
            item.Name = request.Name;
            modelRepository.Update(item);
            return new Unit();
        }
    }
}
