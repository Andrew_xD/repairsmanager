﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Model;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Model
{
    internal class RemoveModelCommand : IRequestHandler<RemoveModelCriteria>
    {
        private readonly IRepository<TVehicleModel> modelRepository;

        public RemoveModelCommand(IRepository<TVehicleModel> modelRepository)
        {
            this.modelRepository = modelRepository;
        }

        public async Task<Unit> Handle(RemoveModelCriteria request, CancellationToken cancellationToken)
        {
            modelRepository.Remove(request.Id);
            return new Unit();
        }
    }
}
