﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Model;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Model
{
    internal class AddModelCommand : IRequestHandler<AddModelCriteria>
    {
        private readonly IRepository<TVehicleModel> modelRepository;
        private readonly IRepository<TVehicleMark> markRepository;

        public AddModelCommand(IRepository<TVehicleModel> modelRepository, IRepository<TVehicleMark> markRepository)
        {
            this.modelRepository = modelRepository;
            this.markRepository = markRepository;
        }

        public async Task<Unit> Handle(AddModelCriteria request, CancellationToken cancellationToken)
        {
            var mark = markRepository.GetItem(request.MarkId);
            var newItem = new TVehicleModel
            {
                Name = request.Name,
                Mark = mark
            };
            modelRepository.Create(newItem);
            return new Unit();
        }
    }
}
