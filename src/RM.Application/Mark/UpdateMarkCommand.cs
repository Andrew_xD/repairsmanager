﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Mark;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Mark
{
    class UpdateMarkCommand : IRequestHandler<UpdateMarkCriteria>
    {
        private readonly IRepository<TVehicleMark> markRepository;

        public UpdateMarkCommand(IRepository<TVehicleMark> markRepository)
        {
            this.markRepository = markRepository;
        }

        public async Task<Unit> Handle(UpdateMarkCriteria request, CancellationToken cancellationToken)
        {
            var item = markRepository.GetItem(request.Id);
            item.Name = request.Name;
            markRepository.Update(item);
            return new Unit();
        }
    }
}
