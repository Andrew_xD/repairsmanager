﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Mark;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Mark
{
    class RemoveMarkCommand : IRequestHandler<RemoveMarkCriteria>
    {
        private readonly IRepository<TVehicleMark> repository;

        public RemoveMarkCommand(IRepository<TVehicleMark> repository)
        {
            this.repository = repository;
        }

        public async Task<Unit> Handle(RemoveMarkCriteria request, CancellationToken cancellationToken)
        {
            repository.Remove(request.Id);
            return new Unit();
        }
    }
}
