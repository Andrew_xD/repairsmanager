﻿using MediatR;
using RM.Application.Contract.Mark;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RM.Application.Mark
{
    internal class GetMarksCommand : IRequestHandler<GetMarksCriteria, IEnumerable<GetMarkResult>>
    {
        private readonly IRepository<TVehicleMark> repository;

        public GetMarksCommand(IRepository<TVehicleMark> repository)
        {
            this.repository = repository;
        }

        public async Task<IEnumerable<GetMarkResult>> Handle(GetMarksCriteria request, CancellationToken cancellationToken)
        {
            return repository.GetItems().Select(x => new GetMarkResult
            {
                Id = x.Id,
                Name = x.Name
            });
        }
    }
}
