﻿using MediatR;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;
using RM.Application.Contract.Mark;
using System.Threading;
using System.Threading.Tasks;

namespace RM.Application.Mark
{
    class AddMarkCommand : IRequestHandler<AddMarkCriteria>
    {
        private readonly IRepository<TVehicleMark> markRepository;

        public AddMarkCommand(IRepository<TVehicleMark> markRepository)
        {
            this.markRepository = markRepository;
        }

        public async Task<Unit> Handle(AddMarkCriteria request, CancellationToken cancellationToken)
        {
            markRepository.Create(new TVehicleMark
            {
                Name = request.Name
            });
            return new Unit();
        }
    }
}
