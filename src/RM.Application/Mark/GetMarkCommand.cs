﻿using MediatR;
using RM.Application.Contract.Mark;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;
using System.Threading;
using System.Threading.Tasks;

namespace RM.Application.Mark
{
    class GetMarkCommand : IRequestHandler<GetMarkCriteriaById, GetMarkResult>
    {
        private readonly IRepository<TVehicleMark> repository;

        public GetMarkCommand(IRepository<TVehicleMark> repository)
        {
            this.repository = repository;
        }

        public async Task<GetMarkResult> Handle(GetMarkCriteriaById request, CancellationToken cancellationToken)
        {
            var item = repository.GetItem(request.Id);
            var result = new GetMarkResult
            {
                Id = item.Id,
                Name = item.Name
            };
            return result;
        }
    }
}
