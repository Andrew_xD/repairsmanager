﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Position;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Position
{
    internal class UpdatePositionCommand : IRequestHandler<UpdatePositionCriteria>
    {
        private readonly IRepository<TPosition> positionRepository;

        public UpdatePositionCommand(IRepository<TPosition> positionRepository)
        {
            this.positionRepository = positionRepository;
        }

        public async Task<Unit> Handle(UpdatePositionCriteria request, CancellationToken cancellationToken)
        {
            var item = positionRepository.GetItem(request.Id);
            item.Name = request.Name;
            positionRepository.Update(item);
            return new Unit();
        }
    }
}
