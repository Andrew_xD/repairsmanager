﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Position;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Position
{
    internal class GetPositionsQuery : IRequestHandler<GetPositionsCriteria, IEnumerable<GetPositionResult>>
    {
        private readonly IRepository<TPosition> positionRepository;

        public GetPositionsQuery(IRepository<TPosition> positionRepository)
        {
            this.positionRepository = positionRepository;
        }

        public async Task<IEnumerable<GetPositionResult>> Handle(GetPositionsCriteria request, CancellationToken cancellationToken)
        {
            return positionRepository.GetItems().Select(x => new GetPositionResult
            {
                Id = x.Id,
                Name = x.Name
            });
        }
    }
}
