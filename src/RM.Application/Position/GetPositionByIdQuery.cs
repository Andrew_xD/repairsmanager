﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Position;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Position
{
    internal class GetPositionByIdQuery : IRequestHandler<GetPositionByIdCriteria, GetPositionResult>
    {
        private readonly IRepository<TPosition> positionRepository;

        public GetPositionByIdQuery(IRepository<TPosition> positionRepository)
        {
            this.positionRepository = positionRepository;
        }

        public async Task<GetPositionResult> Handle(GetPositionByIdCriteria request, CancellationToken cancellationToken)
        {
            var item = positionRepository.GetItem(request.Id);
            var result = new GetPositionResult
            {
                Id = item.Id,
                Name = item.Name
            };
            return result;
        }
    }
}
