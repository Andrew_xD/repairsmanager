﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Position;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Position
{
    internal class RemovePositionCommand : IRequestHandler<RemovePositionCriteria>
    {
        private readonly IRepository<TPosition> positionRepository;

        public RemovePositionCommand(IRepository<TPosition> positionRepository)
        {
            this.positionRepository = positionRepository;
        }

        public async Task<Unit> Handle(RemovePositionCriteria request, CancellationToken cancellationToken)
        {
            positionRepository.Remove(request.Id);
            return new Unit();
        }
    }
}
