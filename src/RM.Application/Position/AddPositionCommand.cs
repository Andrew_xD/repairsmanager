﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Position;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Position
{
    internal class AddPositionCommand : IRequestHandler<AddPositionCriteria>
    {
        private readonly IRepository<TPosition> positionRepository;

        public AddPositionCommand(IRepository<TPosition> positionRepository)
        {
            this.positionRepository = positionRepository;
        }

        public async Task<Unit> Handle(AddPositionCriteria request, CancellationToken cancellationToken)
        {
            positionRepository.Create(new TPosition
            {
                Name = request.Name
            });
            return new Unit();
        }
    }
}
