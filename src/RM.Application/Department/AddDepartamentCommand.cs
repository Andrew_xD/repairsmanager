﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Department;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Department
{
    public class AddDepartamentCommand : IRequestHandler<AddDepartmentCriteria>
    {
        private readonly IRepository<TDepartment> departamentRepository;

        public AddDepartamentCommand(IRepository<TDepartment> departamentRepository)
        {
            this.departamentRepository = departamentRepository;
        }

        public async Task<Unit> Handle(AddDepartmentCriteria request,
            CancellationToken cancellationToken)
        {
            var item = new TDepartment
            {
                Name = request.Name
            };
            departamentRepository.Create(item);
            return new Unit();
        }
    }
}
