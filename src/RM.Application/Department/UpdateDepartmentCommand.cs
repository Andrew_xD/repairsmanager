﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Department;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Department
{
    public class UpdateDepartmentCommand : IRequestHandler<UpdateDepartmentCriteria>
    {
        private readonly IRepository<TDepartment> departamentRepository;

        public UpdateDepartmentCommand(IRepository<TDepartment> departamentRepository)
        {
            this.departamentRepository = departamentRepository;
        }

        public async Task<Unit> Handle(UpdateDepartmentCriteria request, CancellationToken cancellationToken)
        {
            departamentRepository.Update(new TDepartment{ Id = request.Id, Name = request.Name });
            return new Unit();
        }
    }
}
