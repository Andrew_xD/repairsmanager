﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract;
using RM.Application.Contract.Department;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Department
{
    public class GetDepartmentCommand : IRequestHandler<GetDepartmentCriteria, GetDepartmentResult>
    {
        private readonly IRepository<TDepartment> departamentRepository;
        private readonly IRepository<TEmployee> employeeRepository;

        public GetDepartmentCommand(IRepository<TDepartment> departamentRepository, IRepository<TEmployee> employeeRepository)
        {
            this.departamentRepository = departamentRepository;
            this.employeeRepository = employeeRepository;
        }

        public async Task<GetDepartmentResult> Handle(GetDepartmentCriteria request, CancellationToken cancellationToken)
        {
            var item = departamentRepository.GetItem(request.Id);
            return new GetDepartmentResult
            {
                Id = item.Id,
                Name = item.Name,
                Employees = employeeRepository.GetItems(x => x.Department.Id == request.Id).Select(x =>
                    new Employees
                    {
                        Id = x.Id,
                        Name = x.Name
                    })
            };
        }
    }
}
