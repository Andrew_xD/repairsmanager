﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Department;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Department
{
    public class GetDepartmentsCommand : IRequestHandler<GetDepartmentsCriteria, GetDepartmentsResult>
    {
        private readonly IRepository<TDepartment> departamentRepository;

        public GetDepartmentsCommand(IRepository<TDepartment> departamentRepository)
        {
            this.departamentRepository = departamentRepository;
        }

        public async Task<GetDepartmentsResult> Handle(GetDepartmentsCriteria request, CancellationToken cancellationToken)
        {
            var result = departamentRepository
                .GetItems()
                .Select(item => new GetDepartmentResult {Id = item.Id, Name = item.Name})
                .ToList();

            return new GetDepartmentsResult { Departments = result };
        }
    }
}
