﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Department;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Department
{
    public class RemoveDepartmentCommand : IRequestHandler<RemoveDepartmentCriteria>
    {
        private readonly IRepository<TDepartment> departmentRepository;

        public RemoveDepartmentCommand(IRepository<TDepartment> departmentRepository)
        {
            this.departmentRepository = departmentRepository;
        }

        public async Task<Unit> Handle(RemoveDepartmentCriteria request, CancellationToken cancellationToken)
        {
            departmentRepository.Remove(request.Id);
            return new Unit();
        }
    }
}
