﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Vehicle;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Vehicle
{
    internal class GetVehicleByIdQuery : IRequestHandler<GetVehiclesCriteria, IEnumerable<GetVehicleResult>>
    {
        private readonly IRepository<TVehicleModel> modelRepository;

        public GetVehicleByIdQuery(IRepository<TVehicleModel> modelRepository)
        {
            this.modelRepository = modelRepository;
        }

        public async Task<IEnumerable<GetVehicleResult>> Handle(GetVehiclesCriteria request, CancellationToken cancellationToken)
        {
            var items = modelRepository.GetItem(request.ModelId).Vehicles;
            return items.Select(item => new GetVehicleResult { Id = item.Id, Number = item.Number }).ToList();
        }
    }
}
