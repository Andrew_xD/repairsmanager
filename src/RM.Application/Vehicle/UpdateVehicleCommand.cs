﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Vehicle;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Vehicle
{
    public class UpdateVehicleCommand : IRequestHandler<UpdateVehicleCriteria>
    {
        private readonly IRepository<TVehicle> vehicleRepository;

        public UpdateVehicleCommand(IRepository<TVehicle> vehicleRepository)
        {
            this.vehicleRepository = vehicleRepository;
        }

        public async Task<Unit> Handle(UpdateVehicleCriteria request, CancellationToken cancellationToken)
        {
            var item = vehicleRepository.GetItem(request.Id);
            item.Number = request.Number;
            vehicleRepository.Update(item);
            return new Unit();
        }
    }
}
