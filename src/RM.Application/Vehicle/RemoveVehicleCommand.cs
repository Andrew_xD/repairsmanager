﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Vehicle;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Vehicle
{
    internal class RemoveVehicleCommand : IRequestHandler<RemoveVehicleCriteria>
    {
        private readonly IRepository<TVehicle> vehicleRepository;

        public RemoveVehicleCommand(IRepository<TVehicle> vehicleRepository)
        {
            this.vehicleRepository = vehicleRepository;
        }

        public async Task<Unit> Handle(RemoveVehicleCriteria request, CancellationToken cancellationToken)
        {
            vehicleRepository.Remove(request.Id);
            return new Unit();
        }
    }
}
