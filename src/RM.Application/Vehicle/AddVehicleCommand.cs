﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Vehicle;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Vehicle
{
    internal class AddVehicleCommand : IRequestHandler<AddVehicleCriteria>
    {
        private readonly IRepository<TVehicle> vehicleRepository;
        private readonly IRepository<TVehicleModel> vehicleModelRepository;

        public AddVehicleCommand(IRepository<TVehicle> vehicleRepository, IRepository<TVehicleModel> vehicleModelRepository)
        {
            this.vehicleRepository = vehicleRepository;
            this.vehicleModelRepository = vehicleModelRepository;
        }

        public async Task<Unit> Handle(AddVehicleCriteria request, CancellationToken cancellationToken)
        {
            var item = new TVehicle
            {
                Number = request.Number,
                Model = vehicleModelRepository.GetItem(request.ModelId)
            };
            vehicleRepository.Create(item);
            return new Unit();
        }
    }
}
