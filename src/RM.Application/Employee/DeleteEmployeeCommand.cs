﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Employee;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Employee
{
    public class DeleteEmployeeCommand : IRequestHandler<DeleteEmployeeCriteria>
    {
        private readonly IRepository<TEmployee> employeeRepository;

        public DeleteEmployeeCommand(IRepository<TEmployee> employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }

        public async Task<Unit> Handle(DeleteEmployeeCriteria request, CancellationToken cancellationToken)
        {
            employeeRepository.Remove(request.Id);
            return new Unit();
        }
    }
}
