﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Employee;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Employee
{
    public class UpdateEmployeeCommand : IRequestHandler<UpdateEmployeeCriteria>
    {
        private readonly IRepository<TEmployee> employeeRepository;
        private readonly IRepository<TPosition> positionRepository;
        private readonly IRepository<TDepartment> departmentRepository;

        public UpdateEmployeeCommand(
            IRepository<TEmployee> employeeRepository,
            IRepository<TPosition> positionRepository,
            IRepository<TDepartment> departmentRepository)
        {
            this.employeeRepository = employeeRepository;
            this.positionRepository = positionRepository;
            this.departmentRepository = departmentRepository;
        }

        public async Task<Unit> Handle(UpdateEmployeeCriteria request, CancellationToken cancellationToken)
        {
            var pos = positionRepository.GetItem(request.PositionId);
            var dep = departmentRepository.GetItem(request.DepartmentId);
            var emp = employeeRepository.GetItem(request.Id);

            emp.Name = request.Name;
            emp.Department = dep;
            emp.Position = pos;
            employeeRepository.Update(emp);

            return new Unit();
        }
    }
}
