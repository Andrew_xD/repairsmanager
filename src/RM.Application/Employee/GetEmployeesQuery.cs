﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Employee;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Employee
{
    public class GetEmployeesQuery : IRequestHandler<GetEmployeesCriteria, IEnumerable<GetEmployeeResult>>
    {
        private readonly IRepository<TEmployee> employeeRepository;

        public GetEmployeesQuery(IRepository<TEmployee> employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }

        public async Task<IEnumerable<GetEmployeeResult>> Handle(GetEmployeesCriteria request, CancellationToken cancellationToken)
        {
            var items = employeeRepository.GetItems();

            return items.Select(item => new GetEmployeeResult
                {
                    Id = item.Id,
                    Name = item.Name,
                    PositionId = item.Position.Id,
                    PositionName = item.Position.Name,
                    DepartamentId = item.Department.Id,
                    DepartamentName = item.Department.Name
                }).ToList();
        }
    }
}
