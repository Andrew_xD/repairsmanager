﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Employee;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Employee
{
    public class AddEmployeeCommand : IRequestHandler<AddEmployeeCriteria>
    {
        private readonly IRepository<TEmployee> employeeRepository;
        private readonly IRepository<TPosition> positionRepository;
        private readonly IRepository<TDepartment> departmentRepository;

        public AddEmployeeCommand(
            IRepository<TEmployee> employeeRepository,
            IRepository<TPosition> positionRepository,
            IRepository<TDepartment> departmentRepository)
        {
            this.employeeRepository = employeeRepository;
            this.positionRepository = positionRepository;
            this.departmentRepository = departmentRepository;
        }

        public async Task<Unit> Handle(AddEmployeeCriteria request, CancellationToken cancellationToken)
        {
            var pos = positionRepository.GetItem(request.PositionId);
            var dep = departmentRepository.GetItem(request.DepartmentId);

            var model = new TEmployee
            {
                Name = request.Name,
                Department = dep,
                Position = pos
            };
            employeeRepository.Create(model);

            return new Unit();
        }
    }
}
