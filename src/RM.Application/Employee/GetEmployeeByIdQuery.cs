﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using RM.Application.Contract.Employee;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Employee
{
    public class GetEmployeeByIdQuery : IRequestHandler<GetEmployeeByIdCriteria, GetEmployeeResult>
    {
        private IRepository<TEmployee> employeeRepository;

        public GetEmployeeByIdQuery(IRepository<TEmployee> employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }

        public async Task<GetEmployeeResult> Handle(GetEmployeeByIdCriteria request, CancellationToken cancellationToken)
        {
            var item = employeeRepository.GetItem(request.Id);
            return new GetEmployeeResult
            {
                Id = item.Id,
                Name = item.Name,
                PositionId = item.Position.Id,
                PositionName = item.Position.Name,
                DepartamentId = item.Department.Id,
                DepartamentName = item.Department.Name
            };
        }
    }
}
