﻿namespace RM.WebApi.Models
{
    public class MarkView
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
