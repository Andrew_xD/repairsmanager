﻿namespace RM.WebApi.Models
{
    public class EmployeeView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PositionId { get; set; }
        public string PositionName { get; set; }
        public int DepartamentId { get; set; }
        public string DepartamentName { get; set; }
    }
}
