﻿namespace RM.WebApi.Models
{
    public class BaseVehicleView
    {
        public int Id { get; set; }
        public string Number { get; set; }
    }
}
