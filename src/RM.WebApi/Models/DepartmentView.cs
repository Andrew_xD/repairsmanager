﻿using System.Collections.Generic;

namespace RM.WebApi.Models
{
    public class DepartmentView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<EmployeeView> Employees { get; set; }
    }
}
