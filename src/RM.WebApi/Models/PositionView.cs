﻿using System.Collections.Generic;

namespace RM.WebApi.Models
{
    public class PositionView
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
