﻿namespace RM.WebApi.Models
{
    public class BaseModelView
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
