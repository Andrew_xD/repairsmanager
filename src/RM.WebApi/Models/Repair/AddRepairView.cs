﻿using System;

namespace RM.WebApi.Models.Repair
{
    public class AddRepairView
    {
        public int VehicleId { get; set; }
        public int Mileage { get; set; }
        public int DriverId { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
