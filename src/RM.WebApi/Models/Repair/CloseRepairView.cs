﻿using System;

namespace RM.WebApi.Models.Repair
{
    public class CloseRepairView
    {
        public int Id { get; set; }
        public int MasterId { get; set; }
        public DateTime ClosedOn { get; set; }
    }
}
