﻿using System;

namespace RM.WebApi.Models.Repair
{
    public class GetRepairByIdView
    {
        public int Id { get; set; }
        public bool IsClosed { get; set; }

        // Draft repair
        public int VehicleId { get; set; }
        public int DriverId { get; set; }
        public int Mileage { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }

        //Closed repair
        public int? MasterId { get; set; }
        public DateTime ClosedOn { get; set; }
    }
}
