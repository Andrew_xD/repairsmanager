﻿namespace RM.WebApi.Models.Repair
{
    public class UpdateRepairView : AddRepairView
    {
        public int Id { get; set; }
    }
}
