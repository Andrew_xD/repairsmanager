﻿namespace RM.WebApi.Models.Work
{
    public class AddWorkView
    {
        public int Amount { get; set; }
        public string Description { get; set; }
        public int RepairId { get; set; }
    }
}
