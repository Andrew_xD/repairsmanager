﻿namespace RM.WebApi.Models.Work
{
    public class GetWorkByRepairIdView
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }
        public int RepairId { get; set; }
    }
}
