﻿namespace RM.WebApi.Models.Work
{
    public class UpdateWorkView
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }
    }
}
