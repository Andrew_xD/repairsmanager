﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using RM.Application.Contract.Vehicle;
using RM.WebApi.Models;

namespace RM.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly IMediator mediator;

        public VehicleController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        // GET: api/Vehicle/5
        [HttpGet("{id}", Name = "GetVehicleByModel")]
        public async Task<IEnumerable<BaseVehicleView>> Get(int id)
        {
            var items = await mediator.Send(new GetVehiclesCriteria {ModelId = id});
            return items.Select(x => new BaseVehicleView {Id = x.Id, Number = x.Number});
        }

        // POST: api/Vehicle
        [HttpPost]
        public async void Post([FromBody] VehicleView value)
        {
            await mediator.Send(new AddVehicleCriteria { Number = value.Number });
        }

        // PUT: api/Vehicle/5
        [HttpPut]
        public async void Put([FromBody] BaseVehicleView value)
        {
            await mediator.Send(new UpdateVehicleCriteria { Id = value.Id, Number = value.Number });
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async void Delete(int id)
        {
            await mediator.Send(new RemoveVehicleCriteria { Id = id });
        }
    }
}
