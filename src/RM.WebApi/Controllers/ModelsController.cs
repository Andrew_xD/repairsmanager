﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using RM.Application.Contract.Model;
using RM.WebApi.Models;

namespace RM.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModelsController : ControllerBase
    {
        private readonly IMediator mediator;

        public ModelsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        // GET: api/Models
        [HttpGet]
        public async Task<IEnumerable<ModelView>> Get()
        {
            var items = await mediator.Send(new GetModelsCriteria());
            return items.Select(x => new ModelView { Id = x.Id, Name = x.Name, MarkId = x.MarkId });
        }

        // GET: api/Models/5
        [HttpGet("{id}", Name = "GetModelsById")]
        public async Task<BaseModelView> GetModelsById(int id)
        {
            var item = await mediator.Send(new GetModelByIdCriteria {Id = id});
            return new BaseModelView { Id = item.Id, Name = item.Name };
        }

        // POST: api/Models
        [HttpPost]
        public async void Post([FromBody] ModelView value)
        {
            await mediator.Send(new AddModelCriteria { MarkId = value.MarkId, Name = value.Name });
        }

        // PUT: api/Models/5
        [HttpPut]
        public async void Put([FromBody] ModelView value)
        {
            await mediator.Send(new UpdateModelCriteria {Id = value.Id, Name = value.Name});
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async void Delete(int id)
        {
            await mediator.Send(new RemoveModelCriteria { Id = id });
        }
    }
}
