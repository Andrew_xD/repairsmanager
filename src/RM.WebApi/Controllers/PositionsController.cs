﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using RM.Application.Contract.Position;
using RM.WebApi.Models;

namespace RM.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionsController : ControllerBase
    {
        private readonly IMediator mediator;

        public PositionsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        // GET: api/Position
        [HttpGet]
        public async Task<IEnumerable<PositionView>> Get()
        {
            var items = await mediator.Send(new GetPositionsCriteria());
            return items.Select(x => new PositionView
            {
                Id = x.Id,
                Name = x.Name
            });
        }

        // GET: api/Position/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<PositionView> Get(int id)
        {
            var item = await mediator.Send(new GetPositionByIdCriteria {Id = id});
            var result = new PositionView
            {
                Id = item.Id,
                Name = item.Name
            };

            return result;
        }

        // POST: api/Position
        [HttpPost]
        public async void Post([FromBody] PositionView value)
        {
            await mediator.Send(new AddPositionCriteria
            {
                Name = value.Name
            });
        }

        // PUT: api/Position/5
        [HttpPut]
        public async void Put([FromBody] PositionView value)
        {
            await mediator.Send(new UpdatePositionCriteria
            {
                Id = value.Id,
                Name = value.Name
            });
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async void Delete(int id)
        {
            await mediator.Send(new RemovePositionCriteria(id));
        }
    }
}
