﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using RM.Application.Contract.Repair;
using RM.WebApi.Models.Repair;

namespace RM.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RepairsController : ControllerBase
    {
        private readonly IMediator mediator;

        public RepairsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        // GET: api/Repairs/5
        [HttpGet("{id}", Name = "GetRepair")]
        public async Task<GetRepairByIdView> Get(int id)
        {
            var item = await mediator.Send(new GetRepairByIdCriteria { Id = id });

            return new GetRepairByIdView
            {
                Id = item.Id,
                Mileage = item.Mileage,
                VehicleId = item.VehicleId,
                DriverId = item.DriverId,
                Description = item.Description,
                IsClosed = item.IsClosed,
                MasterId = item.MasterId,
                CreatedOn = item.CreatedOn,
                ClosedOn = item.ClosedOn
            };
        }

        // POST: api/Repairs
        [HttpPost]
        public async void Post([FromBody] AddRepairView value)
        {
            var criteria = new AddRepairCriteria
            {
                VehicleId = value.VehicleId,
                DriverId = value.DriverId,
                Description = value.Description,
                Mileage = value.Mileage,
                CreatedOn = value.CreatedOn ?? DateTime.Now
            };
            await mediator.Send(criteria);
        }

        // PUT: api/Repairs/new
        [HttpPut]
        [Route("edit")]
        public async void PutNewRepair([FromBody] UpdateRepairView value)
        {
            var criteria = new UpdateRepairCriteria
            {
                Id = value.Id,
                VehicleId = value.VehicleId,
                DriverId = value.DriverId,
                Description = value.Description,
                Mileage = value.Mileage,
                CreatedOn = value.CreatedOn ?? DateTime.Now
            };
            await mediator.Send(criteria);
        }

        // PUT: api/Repairs/closed
        [HttpPut]
        [Route("close")]
        public async void PutClosedRepair([FromBody] CloseRepairView value)
        {
            var criteria = new CloseRepairCriteria
            {
                Id = value.Id,
                MasterId = value.MasterId,
                ClosedOn = value.ClosedOn
            };
            await mediator.Send(criteria);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async void Delete(int id)
        {
            await mediator.Send(new RemoveRepairCriteria { Id = id });
        }
    }
}
