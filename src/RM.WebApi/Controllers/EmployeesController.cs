﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using RM.Application.Contract.Employee;
using RM.WebApi.Models;

namespace RM.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IMediator mediator;

        public EmployeesController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        // GET: api/Employees
        [HttpGet]
        public async Task<IEnumerable<EmployeeView>> Get()
        {
            var items = await mediator.Send(new GetEmployeesCriteria());
            return items.Select(item => new EmployeeView
            {
                Id = item.Id,
                Name = item.Name,
                PositionId = item.PositionId,
                PositionName = item.PositionName,
                DepartamentId = item.DepartamentId,
                DepartamentName = item.DepartamentName
            });
        }

        // GET: api/Employees/5
        [HttpGet("{id}", Name = "GetEmployee")]
        public async Task<EmployeeView> Get(int id)
        {
            var item = await mediator.Send(new GetEmployeeByIdCriteria {Id = id});
            return new EmployeeView
            {
                Id = item.Id,
                Name = item.Name,
                PositionId = item.PositionId,
                PositionName = item.PositionName,
                DepartamentId = item.DepartamentId,
                DepartamentName = item.DepartamentName
            };
        }

        // POST: api/Employees
        [HttpPost]
        public async void Post([FromBody] EmployeeView value)
        {
            await mediator.Send(new AddEmployeeCriteria
            {
                Name = value.Name,
                DepartmentId = value.DepartamentId,
                PositionId = value.PositionId
            });
        }

        // PUT: api/Employees/5
        [HttpPut]
        public async void Put([FromBody] EmployeeView value)
        {
            await mediator.Send(new UpdateEmployeeCriteria
            {
                Id = value.Id,
                Name = value.Name,
                DepartmentId = value.DepartamentId,
                PositionId = value.PositionId
            });
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async void Delete(int id)
        {
            await mediator.Send(new DeleteEmployeeCriteria
            {
                Id = id
            });
        }
    }
}
