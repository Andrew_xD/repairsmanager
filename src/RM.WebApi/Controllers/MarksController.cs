﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using RM.Application.Contract.Mark;
using RM.WebApi.Models;

namespace RM.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarksController : ControllerBase
    {
        private readonly IMediator mediator;

        public MarksController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        // GET: api/Marks
        [HttpGet]
        public async Task<IEnumerable<MarkView>> Get()
        {
            var items = await mediator.Send(new GetMarksCriteria());
            return items.Select(x => new MarkView
            {
                Id = x.Id,
                Name = x.Name
            });
        }

        // GET: api/Marks/5
        [HttpGet("{id}", Name = "GetMark")]
        public async Task<MarkView> Get(int id)
        {
            var item = await mediator.Send(new GetMarkCriteriaById(id));
            var result = new MarkView { Id = item.Id, Name = item.Name };
            return result;
        }

        // POST: api/Marks
        [HttpPost]
        public async void Post([FromBody] MarkView value)
        {
            await mediator.Send(new AddMarkCriteria { Name = value.Name });
        }

        // PUT: api/Marks/5
        [HttpPut]
        public async void Put([FromBody] MarkView value)
        {
            await mediator.Send(new UpdateMarkCriteria { Id = value.Id, Name = value.Name });
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async void Delete(int id)
        {
            await mediator.Send(new RemoveMarkCriteria(id));
        }
    }
}
