﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using RM.Application.Contract.Department;
using RM.WebApi.Models;

namespace RM.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        private readonly IMediator mediator;

        public DepartmentsController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        // GET: api/Department
        [HttpGet]
        public async Task<IEnumerable<DepartmentView>> Get()
        {
            var item = await mediator.Send(new GetDepartmentsCriteria());
            return item.Departments.Select(x => new DepartmentView
            {
                Id = x.Id,
                Name = x.Name
            });
        }

        // GET: api/Department/5
        [HttpGet("{id}", Name = "GetDepartment")]
        public async Task<DepartmentView> Get(int id)
        {
            var result = await mediator.Send(new GetDepartmentCriteria {Id = id});
            return new DepartmentView
            {
                Id = result.Id,
                Name = result.Name,
                Employees = result.Employees.Select(x => new EmployeeView
                {
                    Id = x.Id,
                    Name = x.Name
                })
            };
        }

        // POST: api/Department
        [HttpPost]
        public async void Post([FromBody] DepartmentView value)
        {
            var criteria = new AddDepartmentCriteria
            {
                Name = value.Name
            };
            await mediator.Send(criteria);
        }

        // PUT: api/Department/5
        [HttpPut]
        public async void Put([FromBody] DepartmentView value)
        {
            var criteria = new UpdateDepartmentCriteria
            {
                Id = value.Id,
                Name = value.Name
            };
            await mediator.Send(criteria);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async void Delete(int id)
        {
            await mediator.Send(new RemoveDepartmentCriteria { Id = id });
        }
    }
}
