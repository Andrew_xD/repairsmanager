﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using RM.Application.Contract.Work;
using RM.WebApi.Models.Work;

namespace RM.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkController : ControllerBase
    {
        private readonly IMediator mediator;

        public WorkController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        // GET: api/Work/5
        [HttpGet("{id}", Name = "GetWorkByRepairId")]
        public async Task<IEnumerable<GetWorkByRepairIdView>> Get(int id)
        {
            var result = await mediator.Send(new GetWorkByRepairIdCriteria(id));
            return result.Select(x => new GetWorkByRepairIdView
            {
                Id = x.Id,
                Amount = x.Amount,
                Description = x.Description,
                RepairId = id
            });
        }

        // POST: api/Work
        [HttpPost]
        public async void Post([FromBody] AddWorkView value)
        {
            await mediator.Send(new AddWorkCriteria
            {
                Amount = value.Amount,
                Description = value.Description,
                RepairId = value.RepairId
            });
        }

        // PUT: api/Work/5
        [HttpPut]
        public async void Put([FromBody] UpdateWorkView value)
        {
            await mediator.Send(new UpdateWorkCriteria
            {
                Id = value.Id,
                Amount = value.Amount,
                Description = value.Description
            });
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async void Delete(int id)
        {
            await mediator.Send(new RemoveWorkCriteria(id));
        }
    }
}
