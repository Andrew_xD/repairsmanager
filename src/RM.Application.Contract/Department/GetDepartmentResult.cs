﻿using System.Collections.Generic;

namespace RM.Application.Contract.Department
{
    public class GetDepartmentResult
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Employees> Employees { get; set; }
    }
}
