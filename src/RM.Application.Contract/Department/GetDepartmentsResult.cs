﻿using System.Collections.Generic;

namespace RM.Application.Contract.Department
{
    public class GetDepartmentsResult
    {   
        public IEnumerable<GetDepartmentResult> Departments { get; set; }
    }
}
