﻿using MediatR;

namespace RM.Application.Contract.Department
{
    public class RemoveDepartmentCriteria : IRequest
    {
        public int Id { get; set; }
    }
}
