﻿using MediatR;

namespace RM.Application.Contract.Department
{
    public class GetDepartmentsCriteria : IRequest<GetDepartmentsResult>
    {
        public int Id { get; set; }
    }
}
