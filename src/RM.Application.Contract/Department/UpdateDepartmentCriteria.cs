﻿using MediatR;

namespace RM.Application.Contract.Department
{
    public class UpdateDepartmentCriteria : IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
