﻿using MediatR;

namespace RM.Application.Contract.Department
{
    public class GetDepartmentCriteria : IRequest<GetDepartmentResult>
    {
        public int Id { get; set; }
    }
}
