﻿using MediatR;

namespace RM.Application.Contract.Department
{
    public class AddDepartmentCriteria : IRequest
    {
        public string Name { get; set; }
    }
}
