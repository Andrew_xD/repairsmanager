﻿using MediatR;

namespace RM.Application.Contract.Mark
{
    public class GetMarkCriteriaById : IRequest<GetMarkResult>
    {
        public GetMarkCriteriaById(int Id = 0)
        {
            this.Id = Id;
        }

        public int Id { get; set; }
    }
}
