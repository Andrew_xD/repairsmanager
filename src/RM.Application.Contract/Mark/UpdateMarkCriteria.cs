﻿using MediatR;

namespace RM.Application.Contract.Mark
{
    public class UpdateMarkCriteria : IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
