﻿namespace RM.Application.Contract.Mark
{
    public class GetMarkResult
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
