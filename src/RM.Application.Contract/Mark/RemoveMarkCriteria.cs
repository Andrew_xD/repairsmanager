﻿using MediatR;

namespace RM.Application.Contract.Mark
{
    public class RemoveMarkCriteria : IRequest
    {
        public RemoveMarkCriteria(int id = 0)
        {
            Id = id;
        }

        public int Id { get; set; }
    }
}
