﻿using MediatR;
using System.Collections.Generic;

namespace RM.Application.Contract.Mark
{
    public class GetMarksCriteria : IRequest<IEnumerable<GetMarkResult>>
    {
    }
}
