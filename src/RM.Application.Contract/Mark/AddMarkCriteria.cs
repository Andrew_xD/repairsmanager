﻿using MediatR;

namespace RM.Application.Contract.Mark
{
    public class AddMarkCriteria : IRequest
    {
        public string Name { get; set; }
    }
}
