﻿using MediatR;

namespace RM.Application.Contract.Position
{
    public class GetPositionResult : IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
