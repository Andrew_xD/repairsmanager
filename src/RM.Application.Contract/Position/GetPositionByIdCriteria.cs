﻿using MediatR;

namespace RM.Application.Contract.Position
{
    public class GetPositionByIdCriteria : IRequest<GetPositionResult>
    {
        public int Id { get; set; }
    }
}
