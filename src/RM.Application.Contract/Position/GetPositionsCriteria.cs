﻿using System.Collections.Generic;
using MediatR;

namespace RM.Application.Contract.Position
{
    public class GetPositionsCriteria : IRequest<IEnumerable<GetPositionResult>>
    {
    }
}
