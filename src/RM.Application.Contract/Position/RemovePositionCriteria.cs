﻿using MediatR;

namespace RM.Application.Contract.Position
{
    public class RemovePositionCriteria : IRequest
    {
        public RemovePositionCriteria(int id = 0)
        {
            Id = id;
        }

        public int Id { get; set; }
    }
}
