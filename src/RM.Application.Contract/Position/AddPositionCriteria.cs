﻿using MediatR;

namespace RM.Application.Contract.Position
{
    public class AddPositionCriteria : IRequest
    {
        public string Name { get; set; }
    }
}
