﻿using MediatR;

namespace RM.Application.Contract.Position
{
    public class UpdatePositionCriteria : IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
