﻿namespace RM.Application.Contract
{
    public class Employees
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PositionId { get; set; }
        public int DepartmentId { get; set; }
    }
}
