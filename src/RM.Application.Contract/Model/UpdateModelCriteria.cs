﻿using MediatR;

namespace RM.Application.Contract.Model
{
    public class UpdateModelCriteria : IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
