﻿using MediatR;

namespace RM.Application.Contract.Model
{
    public class GetModelByIdCriteria : IRequest<GetModelResult>
    {
        public int Id { get; set; }
    }
}
