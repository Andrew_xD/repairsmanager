﻿using MediatR;

namespace RM.Application.Contract.Model
{
    public class AddModelCriteria : IRequest
    {
        public string Name { get; set; }
        public int MarkId { get; set; }
    }
}
