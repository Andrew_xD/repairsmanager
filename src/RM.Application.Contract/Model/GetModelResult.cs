﻿namespace RM.Application.Contract.Model
{
    public class GetModelResult
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int MarkId { get; set; }
    }
}
