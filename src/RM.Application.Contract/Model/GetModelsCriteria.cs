﻿using System.Collections.Generic;
using MediatR;

namespace RM.Application.Contract.Model
{
    public class GetModelsCriteria : IRequest<IEnumerable<GetModelResult>>
    {
    }
}
