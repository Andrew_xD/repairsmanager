﻿using MediatR;

namespace RM.Application.Contract.Model
{
    public class RemoveModelCriteria : IRequest
    {
        public int Id { get; set; }
    }
}
