﻿using MediatR;

namespace RM.Application.Contract.Repair
{
    public class GetRepairByIdCriteria : IRequest<GetRepairByIdResult>
    {
        public int Id { get; set; }
    }
}
