﻿using System;
using MediatR;

namespace RM.Application.Contract.Repair
{
    public class AddRepairCriteria : IRequest
    {
        public int VehicleId { get; set; }
        public int Mileage { get; set; }
        public int DriverId { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
