﻿using MediatR;

namespace RM.Application.Contract.Repair
{
    public class RemoveRepairCriteria : IRequest
    {
        public int Id { get; set; }
    }
}
