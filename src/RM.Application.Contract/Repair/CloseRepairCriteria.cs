﻿using System;
using MediatR;

namespace RM.Application.Contract.Repair
{
    public class CloseRepairCriteria : IRequest
    {
        public int Id { get; set; }
        public int MasterId { get; set; }
        public DateTime ClosedOn { get; set; }
    }
}
