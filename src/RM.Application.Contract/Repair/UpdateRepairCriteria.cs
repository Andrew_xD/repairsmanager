﻿namespace RM.Application.Contract.Repair
{
    public class UpdateRepairCriteria : AddRepairCriteria
    {
        public int Id { get; set; }
    }
}
