﻿using MediatR;

namespace RM.Application.Contract.Work
{
    public class RemoveWorkCriteria : IRequest
    {
        public RemoveWorkCriteria(int id = 0)
        {
            Id = id;
        }

        public int Id { get; set; }
    }
}
