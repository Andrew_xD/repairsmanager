﻿namespace RM.Application.Contract.Work
{
    public class GetWorkByRepairIdResult
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }
    }
}
