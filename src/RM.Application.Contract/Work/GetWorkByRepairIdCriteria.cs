﻿using System.Collections.Generic;
using MediatR;

namespace RM.Application.Contract.Work
{
    public class GetWorkByRepairIdCriteria : IRequest<IEnumerable<GetWorkByRepairIdResult>>
    {
        public GetWorkByRepairIdCriteria(int id = 0)
        {
            Id = id;
        }

        public int Id { get; set; }
    }
}
