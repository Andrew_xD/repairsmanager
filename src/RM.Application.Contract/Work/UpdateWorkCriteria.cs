﻿using MediatR;

namespace RM.Application.Contract.Work
{
    public class UpdateWorkCriteria : IRequest
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }
    }
}
