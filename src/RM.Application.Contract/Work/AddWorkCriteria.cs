﻿using MediatR;

namespace RM.Application.Contract.Work
{
    public class AddWorkCriteria : IRequest
    {
        public int Amount { get; set; }
        public string Description { get; set; }
        public int RepairId { get; set; }
    }
}
