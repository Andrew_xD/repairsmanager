﻿using MediatR;

namespace RM.Application.Contract.Vehicle
{
    public class AddVehicleCriteria : IRequest
    {
        public string Number { get; set; }
        public int ModelId { get; set; }
    }
}
