﻿using System.Collections.Generic;
using MediatR;

namespace RM.Application.Contract.Vehicle
{
    public class GetVehiclesCriteria : IRequest<IEnumerable<GetVehicleResult>>
    {
        public int ModelId { get; set; }
    }
}
