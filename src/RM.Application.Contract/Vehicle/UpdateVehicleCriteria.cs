﻿using MediatR;

namespace RM.Application.Contract.Vehicle
{
    public class UpdateVehicleCriteria : IRequest
    {
        public int Id { get; set; }
        public string Number { get; set; }
    }
}
