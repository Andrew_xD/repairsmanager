﻿namespace RM.Application.Contract.Vehicle
{
    public class GetVehicleResult
    {
        public int Id { get; set; }
        public string Number { get; set; }
    }
}
