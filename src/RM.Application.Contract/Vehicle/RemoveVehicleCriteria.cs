﻿using MediatR;

namespace RM.Application.Contract.Vehicle
{
    public class RemoveVehicleCriteria : IRequest
    {
        public int Id { get; set; }
    }
}
