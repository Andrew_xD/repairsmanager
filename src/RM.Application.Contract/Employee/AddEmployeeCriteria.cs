﻿using MediatR;

namespace RM.Application.Contract.Employee
{
    public class AddEmployeeCriteria : IRequest
    {
        public string Name { get; set; }
        public int PositionId { get; set; }
        public int DepartmentId { get; set; }
    }
}
