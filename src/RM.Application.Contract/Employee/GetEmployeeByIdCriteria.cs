﻿using MediatR;

namespace RM.Application.Contract.Employee
{
    public class GetEmployeeByIdCriteria : IRequest<GetEmployeeResult>
    {
        public int Id { get; set; }
    }
}
