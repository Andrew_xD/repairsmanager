﻿using MediatR;

namespace RM.Application.Contract.Employee
{
    public class DeleteEmployeeCriteria : IRequest
    {
        public int Id { get; set; }
    }
}
