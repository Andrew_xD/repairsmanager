﻿using System.Collections.Generic;
using MediatR;

namespace RM.Application.Contract.Employee
{
    public class GetEmployeesCriteria : IRequest<IEnumerable<GetEmployeeResult>>
    {
    }
}
