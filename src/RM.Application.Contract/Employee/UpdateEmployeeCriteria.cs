﻿namespace RM.Application.Contract.Employee
{
    public class UpdateEmployeeCriteria : AddEmployeeCriteria
    {
        public int Id { get; set; }
    }
}
