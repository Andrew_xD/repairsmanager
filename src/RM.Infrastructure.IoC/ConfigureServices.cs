﻿using System.Reflection;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using RM.Application.Department;
using RM.Application.Dependency.DatabaseRepository;
using RM.Infrastructure.EF;

namespace RM.Infrastructure.IoC
{
    public static class ConfigureServices
    {
        public static void AddServices(this IServiceCollection services)
        {
            services.AddDbContext<RepairsManagerContext>();
            services.AddMediatR(typeof(AddDepartamentCommand).GetTypeInfo().Assembly);
            services.AddScoped(typeof(IRepository<>), typeof(GenericRepository<>));
        }
    }
}
