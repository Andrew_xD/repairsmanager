﻿namespace RM.Infrastructure.EF.Models
{
    public class TVehicle : BaseDataModel
    {
        public string Number { get; set; }
        public TVehicleModel Model { get; set; }
    }
}
