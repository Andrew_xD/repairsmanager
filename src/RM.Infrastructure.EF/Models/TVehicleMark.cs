﻿using System.Collections.Generic;

namespace RM.Infrastructure.EF.Models
{
    public sealed class TVehicleMark : BaseDataModel
    {
        public TVehicleMark()
        {
            Models = new List<TVehicleModel>();
        }

        public string Name { get; set; }
        public ICollection<TVehicleModel> Models { get; private set; }
    }
}
