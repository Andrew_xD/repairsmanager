﻿using System.Collections.Generic;

namespace RM.Infrastructure.EF.Models
{
    public class TDepartment : BaseDataModel
    {
        public TDepartment()
        {
            Employees = new List<TEmployee>();
        }

        public string Name { get; set; }
        public ICollection<TEmployee> Employees { get; set; }
    }
}
