﻿using System;

namespace RM.Infrastructure.EF.Models
{
    public class TRepair : BaseDataModel
    {
        // Common
        public bool IsClosed { get; set; }

        // Draft repair
        public TVehicle Vehicle { get; set; }
        public TEmployee Driver { get; set; }
        public int Mileage { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }

        //Closed repair
        public TEmployee Master { get; set; }
        public DateTime ClosedOn { get; set; }
    }
}
