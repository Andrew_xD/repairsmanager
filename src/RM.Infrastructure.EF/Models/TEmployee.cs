﻿namespace RM.Infrastructure.EF.Models
{
    public class TEmployee : BaseDataModel
    {
        public string Name { get; set; }
        public TPosition Position { get; set; }
        public TDepartment Department { get; set; }
    }
}
