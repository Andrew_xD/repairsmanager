﻿namespace RM.Infrastructure.EF.Models
{
    public class TWork : BaseDataModel
    {
        public int Amount { get; set; }
        public string Description { get; set; }
        public TRepair Repair { get; set; }
    }
}
