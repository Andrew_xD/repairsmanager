﻿using System.Collections.Generic;

namespace RM.Infrastructure.EF.Models
{
    public class TPosition : BaseDataModel
    {
        public TPosition()
        {
            Employees = new List<TEmployee>();
        }

        public string Name { get; set; }
        public ICollection<TEmployee> Employees { get; set; }
    }
}
