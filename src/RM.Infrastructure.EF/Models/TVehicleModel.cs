﻿using System.Collections.Generic;

namespace RM.Infrastructure.EF.Models
{
    public class TVehicleModel : BaseDataModel
    {
        public TVehicleModel()
        {
            Vehicles = new List<TVehicle>();
        }

        public string Name { get; set; }
        public TVehicleMark Mark { get; set; }
        public ICollection<TVehicle> Vehicles { get; private set; }
    }
}
