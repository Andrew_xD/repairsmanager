﻿using Microsoft.EntityFrameworkCore;
using RM.Infrastructure.EF.Models;

namespace RM.Infrastructure.EF
{
    public class RepairsManagerContext : DbContext
    {
        public DbSet<TVehicleMark> VehicleMarks { get; set; }
        public DbSet<TVehicleModel> VehicleModels { get; set; }
        public DbSet<TVehicle> Vehicles { get; set; }

        public DbSet<TEmployee> Employees { get; set; }
        public DbSet<TDepartment> Departments { get; set; }
        public DbSet<TPosition> Positions { get; set; }

        public DbSet<TRepair> Repairs { get; set; }
        public DbSet<TWork> Works { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=RepairsManager.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TVehicleMark>().HasMany(x => x.Models).WithOne(c => c.Mark);
            modelBuilder.Entity<TVehicleModel>().HasMany(x => x.Vehicles).WithOne(c => c.Model);

            modelBuilder.Entity<TPosition>().HasMany(x => x.Employees).WithOne(c => c.Position);
            modelBuilder.Entity<TDepartment>().HasMany(x => x.Employees).WithOne(c => c.Department);

            modelBuilder.Entity<TRepair>().HasOne(x => x.Driver);
            modelBuilder.Entity<TRepair>().HasOne(x => x.Master);
            modelBuilder.Entity<TRepair>().HasOne(x => x.Vehicle);

            modelBuilder.Entity<TWork>().HasOne(x => x.Repair);
        }
    }
}
