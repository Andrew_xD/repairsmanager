﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RM.Infrastructure.EF.Migrations
{
    public partial class AddedVehicleMarkTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VehicleMarks",
                columns: table => new
                {
                    Id = table.Column<int>()
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleMarks", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "VehicleMarks",
                columns: new[] { "Name" },
                values: new object[] { "МАЗ" });

            migrationBuilder.InsertData(
                table: "VehicleMarks",
                columns: new[] { "Name" },
                values: new object[] { "УАЗ" });

            migrationBuilder.InsertData(
                table: "VehicleMarks",
                columns: new[] { "Name" },
                values: new object[] { "Pegue" });

            migrationBuilder.InsertData(
                table: "VehicleMarks",
                columns: new[] { "Name" },
                values: new object[] { "Белаз" });

            migrationBuilder.InsertData(
                table: "VehicleMarks",
                columns: new[] { "Name" },
                values: new object[] { "Амкадор" });

            migrationBuilder.InsertData(
                table: "VehicleMarks",
                columns: new[] { "Name" },
                values: new object[] { "КАМАЗ" });

            migrationBuilder.InsertData(
                table: "VehicleMarks",
                columns: new[] { "Name" },
                values: new object[] { "Reno" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VehicleMarks");
        }
    }
}
