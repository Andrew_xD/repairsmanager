﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RM.Infrastructure.EF.Migrations
{
    public partial class AddedVehicleTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Vehicles",
                columns: table => new
                {
                    Id = table.Column<int>()
                        .Annotation("Sqlite:Autoincrement", true),
                    Number = table.Column<string>(nullable: true),
                    ModelId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vehicles_VehicleModels_ModelId",
                        column: x => x.ModelId,
                        principalTable: "VehicleModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Number", "ModelId" },
                values: new object[] { "1122", 1 });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Number", "ModelId" },
                values: new object[] { "2233", 1 });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Number", "ModelId" },
                values: new object[] { "3344", 1 });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Number", "ModelId" },
                values: new object[] { "5566", 1 });

            migrationBuilder.InsertData(
                table: "Vehicles",
                columns: new[] { "Number", "ModelId" },
                values: new object[] { "7777", 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Vehicles");
        }
    }
}
