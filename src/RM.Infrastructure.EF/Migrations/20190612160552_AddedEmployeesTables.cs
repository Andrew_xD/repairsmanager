﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RM.Infrastructure.EF.Migrations
{
    public partial class AddedEmployeesTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    Id = table.Column<int>()
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Positions",
                columns: table => new
                {
                    Id = table.Column<int>()
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Positions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<int>()
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    PositionId = table.Column<int>(nullable: true),
                    DepartmentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employees_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employees_Positions_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Positions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Employees_DepartmentId",
                table: "Employees",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_PositionId",
                table: "Employees",
                column: "PositionId");

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Name" },
                values: new object[] { "ОМТС" });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Name" },
                values: new object[] { "ГЗС" });

            migrationBuilder.InsertData(
                table: "Positions",
                columns: new[] { "Name" },
                values: new object[] { "Автослесарь" });

            migrationBuilder.InsertData(
                table: "Positions",
                columns: new[] { "Name" },
                values: new object[] { "Водитель" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Name", "PositionId", "DepartmentId" },
                values: new object[] { "Петров И.И.", 1, 1 });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Name", "PositionId", "DepartmentId" },
                values: new object[] { "Иванов А.Д.", 1, 2 });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Name", "PositionId", "DepartmentId" },
                values: new object[] { "Сидоров П.Д.", 2, 2 });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Name", "PositionId", "DepartmentId" },
                values: new object[] { "Чебурашкин А.А.", 2, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropTable(
                name: "Positions");
        }
    }
}
