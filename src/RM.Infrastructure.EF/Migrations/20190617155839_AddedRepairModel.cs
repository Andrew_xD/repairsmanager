﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RM.Infrastructure.EF.Migrations
{
    public partial class AddedRepairModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Repairs",
                columns: table => new
                {
                    Id = table.Column<int>()
                        .Annotation("Sqlite:Autoincrement", true),
                    IsClosed = table.Column<bool>(),
                    VehicleId = table.Column<int>(nullable: true),
                    DriverId = table.Column<int>(nullable: true),
                    Mileage = table.Column<int>(),
                    Description = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(),
                    MasterId = table.Column<int>(nullable: true),
                    ClosedOn = table.Column<DateTime>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Repairs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Repairs_Employees_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Repairs_Employees_MasterId",
                        column: x => x.MasterId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Repairs_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Repairs_DriverId",
                table: "Repairs",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_Repairs_MasterId",
                table: "Repairs",
                column: "MasterId");

            migrationBuilder.CreateIndex(
                name: "IX_Repairs_VehicleId",
                table: "Repairs",
                column: "VehicleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Repairs");
        }
    }
}
