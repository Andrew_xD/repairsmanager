﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RM.Infrastructure.EF.Migrations
{
    public partial class AddedVehicleModelTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VehicleModels",
                columns: table => new
                {
                    Id = table.Column<int>()
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    MarkId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleModels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleModels_VehicleMarks_MarkId",
                        column: x => x.MarkId,
                        principalTable: "VehicleMarks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VehicleModels_MarkId",
                table: "VehicleModels",
                column: "MarkId");

            migrationBuilder.InsertData(
                table: "VehicleModels",
                columns: new[] { "Name", "MarkId" },
                values: new object[] { "1111", 1 });

            migrationBuilder.InsertData(
                table: "VehicleModels",
                columns: new[] { "Name", "MarkId" },
                values: new object[] { "2222", 1 });

            migrationBuilder.InsertData(
                table: "VehicleModels",
                columns: new[] { "Name", "MarkId" },
                values: new object[] { "3333", 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VehicleModels");
        }
    }
}
