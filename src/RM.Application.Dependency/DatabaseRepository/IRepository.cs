﻿using System;
using System.Collections.Generic;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Dependency.DatabaseRepository
{
    public interface IRepository<T> where T: BaseDataModel
    {
        IEnumerable<T> GetItems(Func<T, bool> predicateFunc = null);
        T GetItem(int id);
        void Create(T model);
        void Update(T model);
        void Remove(int id);
    }
}
