﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using RM.Infrastructure.EF;
using RM.Infrastructure.EF.Models;

namespace RM.Application.Dependency.DatabaseRepository
{
    public class GenericRepository<T> : IRepository<T> where T: BaseDataModel
    {
        private readonly RepairsManagerContext context;

        public GenericRepository(RepairsManagerContext context)
        {
            this.context = context;

            context.Employees.Load();
            context.Departments.Load();
            context.Positions.Load();
            context.Repairs.Load();
            context.VehicleMarks.Load();
            context.VehicleModels.Load();
            context.Vehicles.Load();
            context.Works.Load();
        }

        public IEnumerable<T> GetItems(Func<T, bool> predicateFunc = null)
        {
            // ReSharper disable once ArrangeRedundantParentheses
            var tempPredicate = predicateFunc ?? (x => true);
            return context.Set<T>().Where(tempPredicate);
        }

        public T GetItem(int id)
        {
            return context.Set<T>().FirstOrDefault(x => x.Id == id);
        }

        public void Create(T model)
        {
            context.Set<T>().Add(model);
            context.SaveChanges();
        }

        public void Update(T model)
        {
            var item = GetItem(model.Id);
            if (item == null) throw new Exception("Item for remove not found!");
            context.Entry(item).CurrentValues.SetValues(model);
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            var item = GetItem(id);
            if (item == null) throw new Exception("Item for remove not found!");
            context.Set<T>().Remove(item);
            context.SaveChanges();
        }
    }
}
